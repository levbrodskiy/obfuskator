package ru.topteam.obfuscator;

public interface RegexConstants {

    String REGEX_RESERVED_WORDS =
            "(?:byte|short|int|long|char|float|double|boolean" +
                    "|if|else|switch|case|default|while|do|break|continue|for|String" +
                    "|try|catch|finally|throw|throws" +
                    "|private|protected|public" +
                    "|import|package|class|interface|extends|implements|static|final|void|abstract|native" +
                    "|new|return|this|super" +
                    "|synchronized|volatile" +
                    "|const|goto" +
                    "|instanceof|enum|assert|transient|strictfp)";

    String REGEX_DATA_TYPE =
            "(?:String|int|long|float|char|boolean|double|byte|short" +
                    "|Integer|Long|Float|Char|Boolean|Double|Short" +
                    "|String\\[]|int\\[]|long\\[]|float\\[]|char\\[]|boolean\\[]|double\\[]|byte\\[]|short\\[]" +
                    "Integer\\[]|Long\\[]|Float\\[]|Char\\[]|Boolean\\[]|Double\\[]|Short\\[]" +
                    "|InputStream|OutputStream|Thread|Object|" +
                    "List|ArrayList|LinkedList|BufferedReader)";

    String REGEX_FIND_IMPORTS = "([\\w]*\\s?[\\S\\w]*\\W\\s*import\\s[\\w\\.\\;]+)+";

    String REGEX_FIND_BODY_CLASS = "[\\w]*\\s?[\\S\\w]*\\W\\s*import\\s[\\w\\.\\;]+";

    String REGEX_SPECIAL_SYMBOLS = "[\"\\\\\\/\\*+\\.\\,\\(\\)\\&\\?\\%\\$\\#\\@\\!\\;\\№\\{\\}\"]+";

    String REGEX_WORD_AND_PARANTHESS = "\\w*\\[\\]";

    String REGEX_PARANTHESS = "[\\[\\]]";

}
