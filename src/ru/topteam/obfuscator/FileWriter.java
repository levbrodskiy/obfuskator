package ru.topteam.obfuscator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class FileWriter {
    private Path path;
    private String text;

    public FileWriter(Path path, String text){
        this.path = path;
        this.text = text;
    }

    public void write() throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new java.io.FileWriter(new File(path.toUri())))) {
            writer.write(text);
            writer.flush();
        } catch (IOException e) {
            throw new IOException();
        }
    }
}
