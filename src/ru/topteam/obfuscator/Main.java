package ru.topteam.obfuscator;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        Obfuscator obfuscator = new Obfuscator("src//r.java");
        try {
            obfuscator.obfuscating();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Выход за пределы массива");
            e.printStackTrace();
        }
    }
}
