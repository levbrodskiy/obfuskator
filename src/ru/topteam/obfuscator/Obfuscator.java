package ru.topteam.obfuscator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class Obfuscator implements RegexConstants {
    private Path path;
    private String text;
    private String FileName;
    private FileWriter fileWriter;
    private FileReader fileReader;
    private variableNameGenerator variablesNameGenerator = new variableNameGenerator();

    public Obfuscator(String path) {
        this.path = Paths.get(path);
    }

    public void obfuscating() throws IOException {
        fileReader = new FileReader(path);
        text = fileReader.read();
        renameFile();
        deleteComments();
        deleteSpaces();
        renameVariable();
        fileWriter = new FileWriter(path, text);
        fileWriter.write();
    }


    /**
     * Меняет имя файла и содержимое поля text, эквивалентное имени файла
     * на уникальную последовательность символов
     */
    private void renameFile() {
        String replaceFileName = variablesNameGenerator.getUniqueSymbol();
        setFileName(new File(path.toUri()), replaceFileName);
        setClassName(replaceFileName);
    }

    /**
     * Меняет имя файла
     * @param file - файл, имя которого нужно изменить
     * @param replaceFileName - новое имя
     */
    private void setFileName(File file, String replaceFileName) {
        String[] nameAndExpansion = file.getName().split("[.]");
        FileName = nameAndExpansion[0];
        updatePath(file.getParent() + "\\" + replaceFileName + ".java");
        File renameFile = new File(path.toUri());
        file.renameTo(renameFile);
    }

    /**
     * Обновляет путь к файлу
     * @param path - новый путь
     */
    private void updatePath(String path){
        this.path = Paths.get(path);
    }

    /**
     * Заменяет все слова в поле text, эквивалентные имени файла
     * @param replaceFileName - слово для замены
     */
    private void setClassName(String replaceFileName) {
        text = text.replaceAll("\\b" + FileName + "\\b", replaceFileName);
        FileName = replaceFileName;
    }

    private void deleteSpaces() {
        text = text.replaceAll("\\n", "");
        text = text.replaceAll("[\\s]+", " ");
        text = text.replaceAll("[\\s]?[}][\\s]?", "}");
        text = text.replaceAll("[\\s]*[{][\\s]*", "{");

    }

    private void deleteComments() {
        text = text.replaceAll("(?s:/\\*.*?\\*/)|//.*/", "");
    }


    private void renameVariable() {
        String text = this.text;
        String imports = getImports(text);
        String textClass = getBodyClass(text);
        String[] cleanText = cleaningText(textClass);

        Map<String, Integer> vocabulary = makeVarVocabulary(cleanText);
        List<String> listVar = new ArrayList<>(vocabulary.keySet());

        textClass = makeReplacing(cleanText, listVar, textClass);
        text = imports + " " + textClass;
        this.text = text;
    }

    private LinkedHashMap<String, Integer> makeVarVocabulary(String[] allWords) {
        LinkedHashMap<String, Integer> vocabularyList = new LinkedHashMap<>();
        for (int i = 0; i < allWords.length; i++) {

            if ((allWords[i].matches(REGEX_DATA_TYPE)) &&
                    !((allWords[i + 1].matches("[\\s]*"))) &&
                    !(allWords[i + 1].matches(REGEX_RESERVED_WORDS)) &&
                    !(allWords[i - 1].matches(REGEX_RESERVED_WORDS))) {

                vocabularyList.put(allWords[i + 1], i);
            }
        }
        return vocabularyList;
    }

    private String getImports(String text) {
        String blockOfImports = "";
        Pattern p = Pattern.compile(REGEX_FIND_IMPORTS);
        Matcher m = p.matcher(text);
        if (m.find()) {
            blockOfImports = m.group(0);
        }
        return blockOfImports;
    }

    private String getBodyClass(String text) {
        String regex = REGEX_FIND_BODY_CLASS;
        String textClass = text.replaceAll(regex, "");
        return textClass;
    }

    private String[] cleaningText(String text) {
        String textCopy = text.replaceAll(REGEX_SPECIAL_SYMBOLS, " ");
        return textCopy.split("\\s");
    }

    private String makeReplacing(String[] allWords, List<String> listVar, String text) {
        String elementOfVocabulary;
        for (int i = 1; i < allWords.length; i++) {
            for (int j = 0; j < listVar.size(); j++) {

                elementOfVocabulary = getElementWithoutParanthess(listVar.get(j), listVar.get(j));
                allWords[i] = getElementWithoutParanthess(allWords[i], listVar.get(j));
                if ((allWords[i].matches(elementOfVocabulary)) && (!allWords[i].matches("\\S"))) {
                    text = text.replaceAll("\\b" + allWords[i] + "\\b", variablesNameGenerator.getUniqueSymbol());
                }
            }
        }
        return text;
    }

    private String getElementWithoutParanthess(String varriable, String trigger) {
        if (trigger.matches(REGEX_WORD_AND_PARANTHESS)) {
            varriable = varriable.replaceAll(REGEX_PARANTHESS, "");
        }
        return varriable;
    }
}