package ru.topteam.obfuscator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class FileReader {
    private final Path path;

    public FileReader(Path path){
        this.path = path;
    }

    public String read() throws IOException {
        StringBuilder data = new StringBuilder("");
        try (BufferedReader reader = new BufferedReader(new java.io.FileReader(new File(path.toUri())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                data.append(line + " "); //Записывает всё в 1 строчку сразу
            }
        } catch (IOException e) {
            throw new IOException();
        }
        return new String(data);
    }
}
